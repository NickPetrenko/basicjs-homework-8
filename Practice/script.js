//1. Створіть масив з рядків "travel", "hello", "eat", "ski", "lift" та 
//обчисліть кількість рядків з довжиною більше за 3 символи. Вивести це число в консоль.

var strings = ["travel", "hello", "eat", "ski", "lift"];
var count = 0;

for (var i = 0; i < strings.length; i++) {
  if (strings[i].length > 3) {
    count++;
  }
}

console.log(count);



//2. Створіть масив із 4 об'єктів, які містять інформацію про людей: {name: "Іван", age: 25, sex: "чоловіча"}. 
//Наповніть різними даними. Відфільтруйте його, щоб отримати тільки об'єкти зі sex "чоловіча". Відфільтрований 
//масив виведіть в консоль.

var people = [
    { name: "Іван", age: 25, sex: "чоловіча" },
    { name: "Олексій", age: 30, sex: "чоловіча" },
    { name: "Марія", age: 28, sex: "жіноча" },
    { name: "Петро", age: 35, sex: "чоловіча" }
];
  
var filteredPeople = people.filter(function(person) {
    return person.sex === "чоловіча";
});
  
console.log(filteredPeople);



//3. Реалізувати функцію фільтру масиву за вказаним типом даних. (Опціональне завдання)
function filterBy(arr, dataType) {
    return arr.filter(function(item) {
      return typeof item !== dataType;
    });
}
  
var data = ['hello', 'world', 23, '23', null];
var filteredData = filterBy(data, 'string');
console.log(filteredData);